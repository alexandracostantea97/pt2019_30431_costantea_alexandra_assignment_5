import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {

    static DateFormat date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static List<MonitoredData> readFile() {

        String file = "src/Activities.txt";

        List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();

        try {
            monitoredData = Files.lines(Paths.get(file)).map(line ->
            {
                MonitoredData data = new MonitoredData();
                String[] fields = line.trim().split("\\t+");

                String startTimeStr = fields[0];
                Date startTimeDate = null;

                try {
                    startTimeDate = date.parse(startTimeStr);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String endTimeStr = fields[1];
                Date endTimeDate = null;

                try {
                    endTimeDate = date.parse(endTimeStr);

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String activityStr = fields[2];

                data.setStartTime(startTimeDate);
                data.setEndTime(endTimeDate);
                data.setActivityLabel(activityStr);

                return data;
            }).collect(Collectors.toList());

        } catch (
                IOException e) {
            e.printStackTrace();
        }
        return monitoredData;
    }

    public static void task2(List<MonitoredData> list) {
        Set<Integer> dates = list.stream().map(MonitoredData::getStartTime).map(Date::getDate).collect(Collectors.toSet());
        System.out.println("Days of monitored activity: " + dates.size());
    }

    public static void task3(List<MonitoredData> list) {
        Map<String, Integer> stringIntegerMap = new HashMap<>();
        List<String> activities = list.stream().map(MonitoredData::getActivityLabel).collect(Collectors.toList());
        for (String activity : activities.stream().collect(Collectors.toSet())) {
            stringIntegerMap.put(activity, activities.stream().filter(a -> a.contains(activity)).collect(Collectors.toList()).size());
        }
        System.out.println(stringIntegerMap);
    }

    public static void task4(List<MonitoredData> list) {
        Map<Integer, Map<String, Long>> map = list.stream().
                collect(
                        Collectors.groupingBy((MonitoredData i) -> {
                                    return i.getStartTime().getDate();
                                },
                                Collectors.groupingBy((MonitoredData i) -> {
                                            return i.getActivityLabel();
                                        },
                                        Collectors.counting()))
                );
       for(Integer i : map.keySet()){
           for(String s : map.get(i).keySet()){
               System.out.println("Day " + i + " activity " + s + " hapenned " + map.get(i).get(s));
           }
       }
    }

    @SuppressWarnings("unchecked")
    public static void task5(List<MonitoredData> list) {
        List<Pair<String, Long>> ceva = list.stream()
                .map(data -> {
                    Pair<String, Long> pair = new Pair<>(data.getActivityLabel(), (data.getEndTime().getTime() - data.getStartTime().getTime()) / (60 * 1000) % 60);
                    return pair;
                })
                .collect(Collectors.toList());

        for (Pair<String, Long> activity : ceva) {
            System.out.println(activity.getL() + ": " + activity.getR() + "min.");
        }
    }

    public static Long getMinutes(Date startTime, Date endTime) {
        return (endTime.getTime() - startTime.getTime()) / (60 * 1000) % 60;
    }

    public static void task6(List<MonitoredData> list) {
        Map<String, Long> ceva = list.stream()
                .map(data -> {
                    Pair<String, Long> pair = new Pair<>(data.getActivityLabel(), (data.getEndTime().getTime() - data.getStartTime().getTime()) / (60 * 1000) % 60);
                    return pair;
                })
                .collect(Collectors.toList())
                .stream()
                .collect(Collectors.groupingBy(Pair<String, Long>::getL, Collectors.summingLong(Pair<String, Long>::getR)));
        for (String key : ceva.keySet()) {
            System.out.println("activity \"" + key + "\" took a total of" + ceva.get(key) + " minutes");
        }
    }

    public static void task7(List<MonitoredData> list) {
        Function<String, Long> getLessThan5 = p -> {
            return list.stream().filter(monitoredData -> monitoredData.getActivityLabel().equals(p))
                    .filter(monitoredData -> getMinutes(monitoredData.getStartTime(), monitoredData.getEndTime()) < 5)
                    .map(MonitoredData::getActivityLabel)
                    .count();
        };

        Function<String, Long> getAll = p -> {
            return list.stream().filter(monitoredData -> monitoredData.getActivityLabel().equals(p))
                    .map(MonitoredData::getActivityLabel).count();
        };

        List<String> activities = list.stream().map(MonitoredData::getActivityLabel)
                .collect(Collectors.toSet())
                .stream()
                .filter(p -> getLessThan5.apply(p) >= getAll.apply(p) * 0.9).collect(Collectors.toList());

        System.out.println("activities that happen for less than 5 minutes at least 90% of the time:");
        System.out.println(activities);


    }

    public static void main(String args[]) {
        List<MonitoredData> list = readFile();

        System.out.println("Task1");
        task2(list);
        System.out.println("Task2");
        task3(list);
        System.out.println("Task3");
        task4(list);
        System.out.println("Task4");
        task5(list);
        System.out.println("Task5");
        task6(list);
        System.out.println("Task6");
        task7(list);

    }
}
