import java.util.Date;

public class MonitoredData {
    private Date start_time;
    private Date end_time;
    private String activityLabel;

    public MonitoredData(Date start_time, Date end_time, String activityLabel) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.activityLabel = activityLabel;
    }
    public MonitoredData(){

    }

    public Date getStartTime() {
        return start_time;
    }

    public void setStartTime(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEndTime() {
        return end_time;
    }

    public void setEndTime(Date end_time) {
        this.end_time = end_time;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }


    public String toString() {
        return String.format("MonitoredData -> start time: " + getStartTime().toString() + ", end time: " + getEndTime().toString() + ", activity: " + getActivityLabel().toString());

    }

    public Integer getDate(){
        return start_time.getDate();
    }
}
